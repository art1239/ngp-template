import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CardInfoGridComponent } from './card-info-grid.component';

describe('CardInfoGridComponent', () => {
  let component: CardInfoGridComponent;
  let fixture: ComponentFixture<CardInfoGridComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CardInfoGridComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CardInfoGridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
