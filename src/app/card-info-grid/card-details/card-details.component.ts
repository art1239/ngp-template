import { InfoCard } from './../../models/info-card-model';
import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-card-details',
  templateUrl: './card-details.component.html',
  styleUrls: ['./card-details.component.scss']
})
export class CardDetailsComponent implements OnInit {
  @Input() cardData: InfoCard;
  constructor() { }

  ngOnInit(): void {
  }

}
