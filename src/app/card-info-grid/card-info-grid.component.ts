import { Filters } from './../services/filters-service';
import { DataStorageService } from './../shared/data-storage-service';

import { Component, OnInit } from '@angular/core';
import { InfoCard } from '../models/info-card-model';
import * as _ from 'lodash';
import { ThrowStmt } from '@angular/compiler';

@Component({
  selector: 'app-card-info-grid',
  templateUrl: './card-info-grid.component.html',
  styleUrls: ['./card-info-grid.component.scss']
})
export class CardInfoGridComponent implements OnInit {
  public info_cards: InfoCard[] = [];
  private infor_card_filtered: InfoCard[];
  page: number = 1;
  constructor(public dataStorage: DataStorageService, public filters: Filters) {

  }

  ngOnInit(): void {
    this.dataStorage.fetchInfo().subscribe((info: InfoCard[]) => {
      this.info_cards = info;
      this.infor_card_filtered = [...this.info_cards];
    });
    this.filters.onTyped.subscribe((filters) => {

       
      this.info_cards = _.filter(this.infor_card_filtered, _.conforms(filters));
  
    });
  }

  onScroll() {

    this.page += 1;
    this.dataStorage.fetchInfoOnScroll(this.page).subscribe((info: InfoCard[]) => {

      this.info_cards = this.info_cards.concat(info);
    });
  }

  applyFilters() {

  }

  removeFilters() {

  }

}
