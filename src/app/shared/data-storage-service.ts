
import { InfoCard } from './../models/info-card-model';
import { HttpClient } from '@angular/common/http';
import { Injectable } from "@angular/core";

import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';


@Injectable({ providedIn: 'root' })

export class DataStorageService {
    url: string = `http://localhost:3000/info-grids?_limit=15`;
    constructor(private http: HttpClient) {

    }
    getOperator() {
        return this.http.get(this.url).pipe(map((infoCards: InfoCard[]) => {
            return new Set(infoCards.map((infoCard: InfoCard) => {
                return infoCard.operatorName;
            }));
        }));
    }

    fetchInfo(): Observable<InfoCard[]> {

        return this.http.get<InfoCard[]>(this.url);
    }

    fetchInfoOnScroll(page?: number, limit: number = 12) {

        return this.http.get<InfoCard[]>(`http://localhost:3000/info-grids?_page=${page}&_limit=${limit}`);
    }
}