
import { EventEmitter } from "@angular/core";
import * as _ from "lodash";

export class Filters {

    onTyped = new EventEmitter<any>();
    constructor() {

    }
    filters = {};

    public startsWithFilter(property: string, rule?: string) {
        if (rule === '') {
            this.removeFilter(property);
            return;
        }

        this.filters[property] = (val: string) => val.startsWith(rule);


        this.onTyped.emit(this.filters);
    }

    public greaterThanFilter(property: string, rule: any) {
        this.filters[property] = (val: number) => val > rule;
        this.onTyped.emit(this.filters);
    }

    public matchExact(property: string, rule: any) {
        this.filters[property] = (val: string) => val === rule;

        this.onTyped.emit(this.filters);
    }

    public removeFilter(property: string) {
        this.filters[property] = null;
    }



}