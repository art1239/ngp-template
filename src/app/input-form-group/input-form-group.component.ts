import { Filters } from './../services/filters-service';
import { DataStorageService } from './../shared/data-storage-service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-input-form-group',
  templateUrl: './input-form-group.component.html',
  styleUrls: ['./input-form-group.component.scss']
})
export class InputFormGroupComponent implements OnInit {
  operators: Set<String>;

  clientName: string;
  OperatorName: string;
  height: number;
  constructor(private dataStorage: DataStorageService, public filter: Filters) { }

  ngOnInit(): void {
    this.dataStorage.getOperator().subscribe((operators) => {
      this.operators = operators;
    })
  }

  selectedTyped(event: any) {

    this.filter.matchExact('operatorName', event);
  }

  inputTyped(event: string) {

    this.filter.startsWithFilter('clientName', event.toUpperCase());
  }
}
