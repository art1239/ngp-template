import { Filters } from './services/filters-service';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { InputFormGroupComponent } from './input-form-group/input-form-group.component';
import { CardInfoGridComponent } from './card-info-grid/card-info-grid.component';
import { HeaderComponent } from './header/header.component';
import { CardDetailsComponent } from './card-info-grid/card-details/card-details.component';
import { InfoCardService } from './services/info-card-service';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    AppComponent,
    InputFormGroupComponent,
    CardInfoGridComponent,
    HeaderComponent,
    CardDetailsComponent
  ],
  imports: [

    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    InfiniteScrollModule,
    FormsModule,  



  ],
  providers: [InfoCardService,Filters],
  bootstrap: [AppComponent]
})
export class AppModule { }
