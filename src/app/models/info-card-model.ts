export class InfoCard {

    constructor(
        public title: string,
        public clientName: string,
        public operatorName: string,
        public height: number,
        public tags: string[]
    ) {

    }

}